package org.home.example2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class EntityTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Entity entity;

    @Before
    public void setUp() {
        entity = new Entity();
    }

    @Before
    public void setUp2() {
        System.out.println("Test has started");
    }

    @Test
    public void shouldReturnSameNameAsWasSet() {
        entity.setName("Pasha");

        assertEquals("Pasha", entity.getName());
        assertThat(entity.getName(), equalTo("Pasha"));
    }

    @Test
    public void shouldReturnSameIdAsWasSet() {
        entity.setId(1L);

        assertEquals(1L, entity.getId());
    }

    @Test
    public void shouldThrowExceptionIfIdNotValid() {
        thrown.expect(NumberFormatException.class);
        thrown.expectMessage("For input string: \"ABC\"");

        entity.setId("ABC");
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionIfIdNotValid2() {
        entity.setId("ABC");
    }

    @Test
    public void shouldThrowExceptionIfIdNotValid3() {
        try {
            entity.setId("ABC");
            fail("Expected NumberFormatException to be thrown");
        } catch (NumberFormatException ex) {
            assertEquals("For input string: \"ABC\"", ex.getMessage());
        }
    }

    @After
    public void tearDown() {
        System.out.println("Test has finished");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("All tests have finished");
    }
}